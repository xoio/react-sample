/**
 * Text content to go with each image set.
 *
 * (normally it probably makes more sense to have an API send back all the required information, just splitting things
 * to try and find the best balance between automation and having the correct content)
 */
export default [
    {
        title: "Everyday Flowers",
        author: "Johanna Hobel",
        client: "Vouge",
        date: "Jun 2019",
        link: "https://www.google.com"
    },
    {
        title: "The Wilder Night",
        author: "Johanna Hobel",
        client: "Wild",
        date: "Dec 2019",
        link: "https://www.creativeapplications.net/"
    },
    {
        title: "Smooth Memories",
        author: "Johanna Hobel",
        client: "Chanel",
        date: "Feb 2019",
        link: "https://openframeworks.cc/"
    },
    {
        title: "The Future Universe",
        author: "Johanna Hobel",
        client: "On",
        date: "Apr 2019",
        link: "https://libcinder.org/"
    },
    {
        title: "She was born urban",
        author: "Johanna Hobel",
        client: "Si",
        date: "Dec 2021",
        link: "https://nannou.cc/"
    }
]
