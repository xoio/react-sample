import fs from "fs"
import text from "./textcontent.mjs"

/**
 * This file builds an asset manifest based on the images in the directory. Will combine both normal, and it's corresponding
 * retina image into one group for easier tracking. Will output the final path for the client-side code to reference.
 * Also looks in textcontent.mjs to associate the correct content with each image set.
 */

// Path where to find the images
const ASSET_PATH = "./site/assets/images"

// read all the images in the directory
const images = fs.readdirSync(ASSET_PATH)

// the base path where the images will be found once everything is build.
const SITE_PATH = "/images"

// split 2x and 1x images into separate buckets
const normal = [];
const twoXImages = [];
images.forEach(img => {
    if (img.search("@2x") !== -1) {
        twoXImages.push(img)
    } else {
        normal.push(img);
    }
})

// if there are different amounts of images something is wrong.
if (normal.length !== twoXImages.length) {
    throw new Error("Number of normal images differs from 2x images, did you include all of the necessary assets?")
}

// contains all the image information and associates each normal-sized image with it's 2x counterpart.
let image_information = [];

let num_images = normal.length;
for(let i = 0; i < num_images; ++i){
    image_information.push(
        {
            normal:`${SITE_PATH}/${normal[i]}`,
            twoX:`${SITE_PATH}/${twoXImages[i]}`,
            copy:text[i]
        }
    )
}

// Write content to a manifest file
fs.writeFile("./site/src/data.ts", `export default ${JSON.stringify(image_information)}`, err => {
    if (err) {
        throw err;
    }
    console.log("Image information written")
})
