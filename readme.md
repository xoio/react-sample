This is meant to be just a tiny React sample, here I'm building a simple single page site to show off a tiny image gallery.

General
====
* Built using Vite and React
* Scss is chosen due to familiarity 
* Deploys to [Netlify](https://xoio-photo-sample.netlify.app) with each merge into `main` branch. 
* Click on either image in the corners to advance / go back in the stack of images. 
* You can also use up/down to cycle through images
* Lastly, you can also use the mouse wheel to cycle through things, though note that in an effort to not have things cycle too quickly, there is a threshold set up so that a certain amount of force is necessary to trigger the next image.

Setup
===
* `npm install` to install dependencies
* `npm start` to start development mode
* `npm run build` to build for production
* `npm run preview` to see how things should look once deployed. Note that the build process appears to need to run first prior to running preview.

Notes 
====
* Content is compiled instead of hard-coded. 
  * `assetbuild.mjs` reads the images in the assets directory and pairs each normal image with its retina counterpart. 
  * It will also associate the correct text content(in `textcontent.mjs`) with each image pair. 
* Note that this was built on Windows so there was no easy way to test out OSX and it's potentially backwards scrolling situation. 
  * I don't have an Unix machine to test the build script with but it presumably should work the same as Netlify seems to have no problem building the project.
* In development mode, you may see suggestions to rename paths, this shouldn't affect anything; paths are kept the way they are simply to avoid the IDE from showing errors.
