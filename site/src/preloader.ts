/**
 * Preloads images
 * @param content {Array} an array of data content to load.
 */
export default async function (content) {
    let promises = [];

    content.forEach((itm, i) => {

        promises.push({
            id: i,
            twoX: false,
            img: new Promise((res, rej) => {
                let image = new Image();
                image.src = itm.normal
                image.onload = () => {
                    res(image);
                }
                image.onerror = e => {
                    rej(e)
                }
            })
        })

        promises.push({
            id: i,
            twoX: true,
            img: new Promise((res, rej) => {
                let image = new Image();
                image.src = itm.twoX
                image.onload = () => {
                    res(image);
                }
                image.onerror = e => {
                    rej(e)
                }
            })
        })
    })

    // Resolve promises and re-associate each normal image with it's 2x counterpart.
    let images = await Promise.all(promises);

    let image_groups = [];
    let i = 0;
    for (const obj of images) {

        let img = null;
        let twoX = null;

        // For all non 2X images, resolve the Promise, look for the 2x counterpart and write to group array.
        if (!obj.twoX) {
            img = await obj.img

            // look for 2x version
            for (const twoXObj of images) {
                if (twoXObj.id === obj.id) {
                    twoX = await twoXObj.img
                }
            }

            image_groups.push({
                normal: img,
                twoX: twoX,
                text:content[i].copy,
                width:img.width,
                height:img.height,
                twoXWidth:twoX.width,
                twoXHeight:twoX.height,
                id: obj.id
            })

            i++;
        }
    }

    return image_groups
}
