/**
 * Holds various event properties to look out for.
 */
const settings = {
    // holds which key codes are valid.
    key_codes: {
        "up": 38,
        "down": 40
    }
}

/**
 * Figures out the scroll information including direction and whether the threshold was reached.
 * @param e {Event} the event from the scroll listener.
 */
export function calculate_scroll_data(e) {
    let deltaY = e.deltaY * -1

    return {
        direction: deltaY < 0 ? "down" : "up",
        can_trigger: deltaY > 120 || deltaY < -120
    }
}

/**
 * Determines the correct scroll event name to use when adding an event listener. Used as different browsers may use
 * different names.
 */
export function get_scroll_event() {
    let event_name = ""

    // Possible event names for scroll wheel event
    let events = ["onwheel", "onmousewheel", "scroll"]

    // loop through all possible events. Take the last one that exists.
    events.forEach((evt, i) => {
        if (evt in document) {
            event_name = evt
        }
    })

    return event_name
}

export default settings;