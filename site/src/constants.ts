export default {
    // indices for each featured image to use when referencing values in the indices array
    PREVIOUS_IMAGE_INDEX: 0,
    CURRENT_IMAGE_INDEX: 1,
    NEXT_IMAGE_INDEX: 2,

    // base duration to feed to GSAP as to how long to run the main animation for
    ANIM_OUT_DURATION: 2,

    // base duration to feed to GSAP as to how long to run the fade in animation for
    ANIM_IN_DURATION: 2,

    // Value to delay timing by when fading things back in.
    ANIM_IN_DELAY() {
        return (1000 * this.ANIM_OUT_DURATION) + 2000
    },

    // class name to apply to <body> to help all components know when something is being animated.
    ANIM_LOCK_CLASS_NAME: "animating",

    // class name to use for elements that fade in.
    CSS_FADE_IN_CLASS: "fade_in"
}