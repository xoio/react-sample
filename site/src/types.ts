
// Basic encapsulation of properties for an image, just width and height in this case.
interface ImageDimensions {
    width: number
    height: number
}