// figma design https://www.figma.com/file/vQw32zU0ZnFEYXkAcsSekC/wild-dev-challenge
import {createRoot} from "react-dom/client"
import * as React from "react"
import ImageData from "./data"
import Preloader from "./preloader"
import Page from "./components/page"

//@ts-ignore
import styles from "../styles/main.scss"

// preload any thumbnail data.
(async function () {

    let data = await Preloader(ImageData);

    createRoot(document.querySelector("#APP")).render(
        <Page images={data}/>
    )
})()