import * as React from "react";

// Checks to see if the screen is possibly a retina screen.
export function isRetina(): Boolean {
    if (window.matchMedia) {
        let mq = window.matchMedia("only screen and (min--moz-device-pixel-ratio: 1.3), only screen and (-o-min-device-pixel-ratio: 2.6/2), only screen and (-webkit-min-device-pixel-ratio: 1.3), only screen  and (min-device-pixel-ratio: 1.3), only screen and (min-resolution: 1.3dppx)");
        return (mq && mq.matches)
    } else {
        return window.devicePixelRatio > 1
    }
}

/**
 * Returns a random integer value between two numbers
 * @param min {Number} the minimum value
 * @param max {Number} the maximum value
 * @returns {number}
 */
export function randInt(min: number = 0, max: number = 1) {
    return Math.floor(min + Math.random() * (max - min + 1));
}

/**
 * Splits a string into individual chars
 * @param text {string} the text to split
 * @param id {string} the class name to use for all the chars to help differentiate a split string from another.
 */
export function splitText(text:String, id:string = "char_split"){
    let chars = text.split("");

    return chars.map((itm,i) => {
        return (
            <span key={i} className={`split_text_char ${id}`}>itm</span>
        )
    })
}

/**
 * Used in animations, builds sets of random multipliers to use during animation to help generate a random
 * direction to animate to.
 *
 * @param num_sets {number} the number of sets to generate
 * @param offset {number} adjusts the intensity of how far an object will shift in a given direction
 */
export function buildRandomDirections(num_sets: number, offset: number = 2) {
    let directions = [];

    for (let i = 0; i < num_sets; ++i) {

        // 0 = up, 1 = down, 2 = left, 3 = right
        let direction = randInt(0, 4);
        let xMult = 1;
        let yMult = 1;

        switch (direction) {
            case 0:
                yMult = -offset
                break;

            case 1:
                yMult = offset
                break;

            case 2:
                xMult *= -offset
                break;

            case 3:
                xMult *= offset
                break;
        }

        directions.push([xMult, yMult])
    }
    return directions;
}