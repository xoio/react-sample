import * as React from "react";
import {useEffect, useMemo, useRef, useState} from "react";

/**
 * Manages the element that follows the cursor indicating what image we're looking at
 * @param props props for the component. Should include [max_images] indicating the maximum number of images as well as the
 * [current_index] we're currently on.
 */
export default function (props) {

    const {
        max_images = 1,
        current_index = 0
    } = props

    function toRadians(deg) {
        return (deg * Math.PI) / 180;
    }

    // start from 270 deg so 0 is at the top.
    const zero_angle = useMemo(() => {
        return toRadians(270);
    }, [])

    const canvas_ref = useRef()
    const animate_ref = useRef();

    // context state
    const [ctx, setCtx] = useState(null)

    // holds the value of where the stroke should be.
    const [stroke_amt, set_stroke_amt] = useState(0)

    // initial setup
    useEffect(() => {
        setCtx(canvas_ref.current.getContext("2d"))
        set_stroke_amt(zero_angle + toRadians(360 / max_images) * (current_index + 1))

        function on_mouse_move(e) {
            canvas_ref.current.style.transform = `translate3d(${e.pageX}px,${e.pageY}px,0)`
        }

        window.addEventListener("mousemove", on_mouse_move)

        return () => {
            window.removeEventListener("mousemove", on_mouse_move)
        }
    }, [])

    // adjust stroke value when current index changes.
    useEffect(() => {
        let new_val = (zero_angle + toRadians(360 / max_images) * (current_index + 1))
        set_stroke_amt(new_val)
    }, [current_index])

    // Run animation.
    useEffect(() => {
        animate_ref.current = requestAnimationFrame(animate)

        return () => {
            cancelAnimationFrame(animate_ref.current)
        }
    }, [ctx, stroke_amt])

    /**
     * Animation loop for the counter.
     * (had planned to try to animate stroke but running out of time)
     */
    function animate() {
        animate_ref.current = requestAnimationFrame(animate)

        if (ctx !== null) {

            ctx.clearRect(0, 0, 200, 200)

            let pos = {
                x: 80,
                y: 75
            }

            // draw complete circle.
            ctx.beginPath();
            ctx.arc(pos.x, pos.y, 50, zero_angle, 360);
            ctx.lineWidth = 1
            ctx.strokeStyle = "rgba(255,255,255,5)"
            ctx.stroke();

            // draw the arc indicating how many of the photo's we've progressed through.
            ctx.beginPath();
            ctx.arc(pos.x, pos.y, 50, zero_angle, stroke_amt);
            ctx.strokeStyle = "#fff"
            ctx.lineWidth = 5
            ctx.stroke();

            // draw center dot.
            ctx.beginPath();
            ctx.arc(pos.x, pos.y, 5, zero_angle, 360);
            ctx.fillStyle = "#fff"
            ctx.fill()
        }
    }

    return (
        <canvas ref={canvas_ref} id={"counter_addon"}></canvas>
    )
}