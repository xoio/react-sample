import * as React from "react";
import events from "../events";
import {useEffect, useRef, useState} from "react";
import constants from "../constants";
import {fadeElementOutIn} from "../animation";

/**
 * Component to create metadata information like author, client and date
 * @param props
 */
export default function(props){
    const {
        imageData,
        fadeInTimeout = (constants.ANIM_OUT_DURATION * 1000) + 1000,
        is_transitioning = false
    } = props

    const metaRef = useRef();

    useEffect(()=>{
        metaRef.current.classList.add("fade_in");
    },[])

    useEffect(() => {
        if (is_transitioning) {
            fadeElementOutIn(metaRef, fadeInTimeout, "fade_in")
        }
    }, [is_transitioning])

    function click(){
        window.open(imageData.text.link,"target=_blank")
    }

    return (
        <>
            <section className={"metadata"} ref={metaRef}>
                <p className={"author"}>{imageData.text.author}</p>
                <p className={"client"}>For {imageData.text.client}</p>
                <p className={"date"}>{imageData.text.date}</p>
                <button className={"cta_button"} onClick={click}>Have a look</button>
            </section>
        </>
    )
}