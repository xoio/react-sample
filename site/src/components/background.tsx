import * as React from "react"
import {useEffect, useRef, useState} from "react";
import constants from "../constants";
import events from "../events";
import {fadeElementOutIn} from "../animation";

/**
 * Constructs the background image
 * @param props {Object} should get passed the property "image" which should be an object containing the id and image elements that have
 * been selected.
 */
export default function (props) {

    const {
        image,
        fadeInTimeout = (constants.ANIM_OUT_DURATION * 1000) + 1000,
        is_transitioning = false
    } = props

    const imgRef = useRef()

    // Initial fade in
    useEffect(() => {
        imgRef.current.classList.add("fade_in");
    }, [])

    // Trigger animation when parent says it's ok to animate.
    useEffect(() => {
        if (is_transitioning) {
            fadeElementOutIn(imgRef, fadeInTimeout, constants.CSS_FADE_IN_CLASS)
        }
    }, [is_transitioning])

    return (
        <div className="background_image" ref={imgRef}>
            <img src={image.twoX.src} alt={`Background for image ${image.id}`}/>
        </div>
    )
}