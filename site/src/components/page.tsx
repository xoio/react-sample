import * as React from "react"
import Background from "./background"
import {useEffect, useMemo, useRef, useState} from "react";
import Image from "./image";
import gsap from "gsap";
import ImageHelpers from "../image_helpers"
import {isRetina} from "../utils";
import constants from "../constants";
import {animateOut, animateIn} from "../animation"
import Counter from "./counter"
import Metadata from "./metadata";
import events, {calculate_scroll_data, get_scroll_event} from "../events";


/**
 * Lays out the scene / grid. General format is as follows
 *
 * - lower left corner image is "previous"
 * - middle is "current"
 * - top right is "next"
 *
 * Other images get hidden off-screen.
 *
 * @param props
 */
export default function (props) {

    const {images} = props;

    const MAX_IMAGES = images.length;

    // We assume all the images are the same size both normally and in 2x size, so store that value since it
    // will be useful in helping to calculate position.
    const EXPANDED_DIMENSIONS = {
        width: isRetina() ? images[0].twoXWidth : images[0].width,
        height: isRetina() ? images[0].twoXHeight : images[0].height
    }

    // the amount of time to take to animate things out. Value in seconds.
    const ANIM_OUT_DURATION = 2

    /**
     * Keeps track of featured images and the index of what image information needs to be grabbed for each section.
     *
     * 0 = previous image
     * 1 = current image
     * 2 = next image
     */
    const [indices, setIndices] = useState([MAX_IMAGES - 1, 0, 1])

    // The current "expanded" image that's featured.
    const [current_index, set_current_index] = useState(0);

    let is_animating = false;

    const [is_transitioning, set_transition_state] = useState(false)

    const currentRef = useRef()
    const nextRef = useRef()
    const previousRef = useRef()
    const textRef = useRef()

    // Build an array of the above refs so it's simpler to assign reference to proper position.
    const refs = useMemo(() => {
        return [
            previousRef,
            currentRef,
            nextRef
        ]
    }, [currentRef, nextRef, previousRef])

    // add some general events
    useEffect(() => {
        let onKeyDown = e => {

            // ignore keypresses that aren't up or down.

            if (e.keyCode === events.key_codes.up || e.keyCode === events.key_codes.down) {
                if (!is_animating) {
                    is_animating = true;
                    set_transition_state(true)
                    document.body.classList.add(constants.ANIM_LOCK_CLASS_NAME);
                    animateOut(EXPANDED_DIMENSIONS, textRef, refs, () => {
                        if (e.keyCode === events.key_codes.up) {
                            incrementImages()
                        } else if (e.keyCode === events.key_codes.down) {
                            decrementImages();
                        }
                    })

                    // after 1 sec, animate in
                    setTimeout(() => {
                        animateIn(EXPANDED_DIMENSIONS, textRef, refs, () => {
                            is_animating = false;
                            set_transition_state(false)
                            document.body.classList.remove(constants.ANIM_LOCK_CLASS_NAME);
                        });

                    }, constants.ANIM_IN_DELAY())
                }
            } else {
                return;
            }
        }

        let onResize = e => {
            updateLayout()
        }

        let onScroll = e => {
            let data = calculate_scroll_data(e)

            // We only run if the scroll delta has been reached.
            if (data.can_trigger && !is_animating) {
                is_animating = true;
                set_transition_state(true)
                document.body.classList.add(constants.ANIM_LOCK_CLASS_NAME);
                animateOut(EXPANDED_DIMENSIONS, textRef, refs, () => {
                    if (data.direction === "up") {
                        incrementImages()
                    } else {
                        decrementImages();
                    }
                })

                // after 1 sec, animate in
                setTimeout(() => {
                    animateIn(EXPANDED_DIMENSIONS, textRef, refs, () => {
                        is_animating = false;
                        set_transition_state(false)
                        document.body.classList.remove(constants.ANIM_LOCK_CLASS_NAME);
                    });

                }, constants.ANIM_IN_DELAY())
            }
        }

        window[get_scroll_event()] = onScroll
        window.addEventListener("keydown", onKeyDown)
        window.addEventListener("resize", onResize)

        return () => {
            window.removeEventListener("keydown", onKeyDown)
            window.removeEventListener("resize", onResize)
            window.removeEventListener("wheel", onScroll);
        }
    }, [])

    // initial image layout
    useEffect(() => {
        updateLayout()
    }, [])

    // update the current featured index when the indices change.
    useEffect(() => {
        set_current_index(indices[1])
    }, [indices])

    /**
     * Callback function to run if an image is clicked on.
     * @param id the index id of the Image component that was just clicked on.
     */
    function on_click(id) {

        if (is_transitioning) {
            return
        }

        if (!is_animating) {
            is_animating = true;
            set_transition_state(true)
            document.body.classList.add(constants.ANIM_LOCK_CLASS_NAME);
            animateOut(EXPANDED_DIMENSIONS, textRef, refs, () => {
                if (id > current_index) {
                    incrementImages()
                } else {
                    decrementImages();
                }
            })

            // after 1 sec, animate in
            setTimeout(() => {
                animateIn(EXPANDED_DIMENSIONS, textRef, refs, () => {
                    is_animating = false;
                    set_transition_state(false)
                    document.body.classList.remove(constants.ANIM_LOCK_CLASS_NAME);
                });

            }, constants.ANIM_IN_DELAY())
        }
    }

    /**
     * increments current featured image indices by one.
     */
    function incrementImages() {

        indices[0] = ImageHelpers.getNextIndex(indices[constants.PREVIOUS_IMAGE_INDEX], MAX_IMAGES)
        indices[1] = ImageHelpers.getNextIndex(indices[constants.CURRENT_IMAGE_INDEX], MAX_IMAGES)
        indices[2] = ImageHelpers.getNextIndex(indices[constants.NEXT_IMAGE_INDEX], MAX_IMAGES)

        setIndices([...indices])
    }

    /**
     * decrements current featured image indices by one.
     */
    function decrementImages() {
        indices[0] = ImageHelpers.getPreviousIndex(indices[constants.PREVIOUS_IMAGE_INDEX], MAX_IMAGES)
        indices[1] = ImageHelpers.getPreviousIndex(indices[constants.CURRENT_IMAGE_INDEX], MAX_IMAGES)
        indices[2] = ImageHelpers.getPreviousIndex(indices[constants.NEXT_IMAGE_INDEX], MAX_IMAGES)

        setIndices([...indices])
    }

    /**
     * Updates the layout of the feature images.
     */
    function updateLayout() {
        let normal_dimensions = ImageHelpers.getNormalDimensions(EXPANDED_DIMENSIONS)
        let previous_position = ImageHelpers.getPreviousPosition(EXPANDED_DIMENSIONS)
        let next_position = ImageHelpers.getNextPosition(EXPANDED_DIMENSIONS)
        let current_position = ImageHelpers.getCurrentPosition(EXPANDED_DIMENSIONS)

        gsap.to(refs[constants.PREVIOUS_IMAGE_INDEX].current, {
            width: normal_dimensions.width,
            height: normal_dimensions.height,
            x: previous_position.x,
            y: previous_position.y,
            duration: 0
        })
        gsap.to(refs[constants.CURRENT_IMAGE_INDEX].current, {
            width: EXPANDED_DIMENSIONS.width,
            height: EXPANDED_DIMENSIONS.height,
            x: current_position.x,
            y: current_position.y,
            duration: 0
        })

        gsap.to(refs[constants.NEXT_IMAGE_INDEX].current, {
            width: normal_dimensions.width,
            height: normal_dimensions.height,
            x: next_position.x,
            y: next_position.y,
            duration: 0
        })
    }

    return (
        <main>
            <Background image={images[indices[1]]} is_transitioning={is_transitioning}/>

            <h1 className={"site_title"}>XYZ Photography</h1>


            <section className={"image_container"}>

                <h1 ref={textRef} className={"title_text"}>{images[current_index].text.title}</h1>
                {indices.map((number, i) =>
                    <Image
                        on_click={on_click}
                        featured={i == constants.CURRENT_IMAGE_INDEX}
                        imageData={images[indices[i]]}
                        ref={refs[i]}
                        id={i}
                        key={i}/>
                )}

                <Counter current_index={current_index} max_images={MAX_IMAGES}/>
                <Metadata imageData={images[current_index]} is_transitioning={is_transitioning}/>
            </section>

        </main>
    )
}