import * as React from "react"
import {useMemo} from "react";
import CursorAddon from "./cursor_addon"

/**
 * This constructs the counter element indicating which image the user is viewing out of x number of images.
 * @param props - should take [current_index] indicating the current index that should be displayed. Should also take [max_images]
 * indicating the total number of images.
 */
export default function (props) {

    const {
        current_index,
        max_images = 1
    } = props

    // Build number of dots to help indicate position.
    const dots = useMemo(() => {
        let dot_elements = [];
        for (let i = 0; i < max_images; ++i) {
            dot_elements.push(i)
        }
        return dot_elements
    }, [props])

    return (
        <>
            <CursorAddon current_index={current_index} max_images={max_images}/>
            <section id={"counter"}>

                <div className={"counter_text"}>
                    {current_index + 1} of {max_images}
                </div>

                <div className={"counter_dots"}>
                    {dots.map((number, i) =>
                        <span className={`counter_dot ${current_index === i ? "active" : ""}`} key={i}></span>
                    )}
                </div>
            </section>
        </>
    )
}