import * as React from "react";
import {useEffect} from "react";
import gsap from "gsap";

/**
 * Responsible for displaying an image.
 * @param imageData {Object} the image data for the component. Should contain the normal image, the 2x image and the id for the image.
 * @param featured {boolean} whether or not this component instance is the featured image.
 */
const Image = React.forwardRef((props, ref) => {

    const {
        imageData,
        featured = false,
        on_click = () => {
        },
        id = 0
    } = props

    // animate the images in when initially building component.
    useEffect(() => {
        gsap.to(ref.current, {
            opacity: 1,
            duration: 1
        })
    }, [])

    /**
     * Handles clicks. Only runs onClick callback if Image is not featured.
     */
    function click() {
        if (!featured) {
            on_click(id)
        }
    }

    return (
        <>
            <div data-id={id} className={`image_object ${featured === true ? "featured" : ""}`} ref={ref}
                 onClick={click}>
                <h1 className={featured === true ? "featured" : ""}>{imageData.text.title}</h1>
                <img onClick={click} srcSet={`${imageData.normal.src} 1x, ${imageData.twoX.src} 2x`}/>
            </div>
        </>
    )

});

export default Image;