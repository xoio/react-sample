
export default {

    // the scale value to use when setting the dimensions for the "normal" sized images.
    normalStateScaleValue: 2,

    // padding of where to place the images
    padding: 20,

    /**
     * Returns the dimensions for a "normal" image.
     * @param dimensions {ImageDimensions} any object with a width / height prop which is then divided by the scale to
     * get the correct dimensions for the image.
     */
    getNormalDimensions(dimensions: ImageDimensions) {
        return {
            width: dimensions.width / this.normalStateScaleValue,
            height: dimensions.height / this.normalStateScaleValue
        }
    },

    /**
     * Get the next index - used when we animate to the next/previous image.
     * @param current_index {number} the current index we're on
     * @param max {number} the maximum number of images available.
     */
    getNextIndex(current_index: number, max: number) {
        if (current_index < max - 1) {
            return current_index += 1;
        } else {
            return 0
        }
    },

    /**
     * Get the previous index - used when we animate to the next/previous image.
     * @param current_index {number} the current index we're on
     * @param max {number} the maximum number of images available.
     */
    getPreviousIndex(current_index: number, max: number) {
        if (current_index > 0) {
            return current_index -= 1;
        } else {
            return max - 1;
        }
    },

    /**
     * Returns the coordinates of where to position the component representing the current element
     * @param dimensions {ImageDimensions} the width / height of the component so we can position it properly on screen.
     */
    getCurrentPosition(dimensions: ImageDimensions) {
        return {
            x: (window.innerWidth / 2) - (dimensions.width / 2),
            y: (window.innerHeight / 2) - (dimensions.height / 2),
            z: 0
        }
    },

    /**
     * Returns the coordinates of where to position the component representing the next element
     * @param dimensions {ImageDimensions} the width / height of the component, so we can position it properly on screen.
     */
    getNextPosition(dimensions: ImageDimensions) {
        let normal_dimensions = this.getNormalDimensions(dimensions)
        return {
            x: (window.innerWidth) - (normal_dimensions.width) - this.padding,
            y: this.padding,
            z: 0
        }
    },

    /**
     * Returns the coordinates of where to position the component representing the previous element
     * @param dimensions {ImageDimensions} the width / height of the component, so we can position it properly on screen.
     */
    getPreviousPosition(dimensions: ImageDimensions) {
        let normal_dimensions = this.getNormalDimensions(dimensions)
        return {
            x: this.padding,
            y: (window.innerHeight) - (normal_dimensions.height) - this.padding,
            z: 0
        }
    }
}