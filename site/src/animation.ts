import gsap from "gsap";
import ImageHelpers from "./image_helpers";
import {Ref} from "react";
import {buildRandomDirections} from "./utils";
import constants from "./constants";

/**
 * Animates content out.
 * @param dimensions {ImageDimension} the dimensions of the main "featured" image.
 * @param textRef {Ref} the ref object for the main text
 * @param refs {Array<Ref>} an array of references that correspond with all the featured images.
 * @param onComplete {Function} the function to run once the animation is complete.
 */
export function animateOut(
    dimensions: ImageDimension,
    textRef: Ref<any>,
    refs: Array<Ref<any>>,
    cb: Function
) {
    let previous_position = ImageHelpers.getPreviousPosition(dimensions)
    let next_position = ImageHelpers.getNextPosition(dimensions)
    let current_position = ImageHelpers.getCurrentPosition(dimensions)

    // TODO is it better to just store current position on ref directly so we don't have to do separate calls?
    let completeCount = 0;

    // animate main text out
    gsap.to(textRef.current, {
        opacity: 0,
        delay: constants.ANIM_OUT_DURATION * 0.5,
        duration: constants.ANIM_OUT_DURATION,
        onComplete() {
            completeCount++;
        }
    })

    // build a directional multiplier to allow for a random direction of animation
    let directions = buildRandomDirections(3);

    gsap.to(refs[constants.PREVIOUS_IMAGE_INDEX].current, {
        x: previous_position.x + (Math.random() * 100) * directions[constants.PREVIOUS_IMAGE_INDEX][0],
        y: previous_position.y + (Math.random() * 100) * directions[constants.PREVIOUS_IMAGE_INDEX][1],
        opacity: 0,
        duration: constants.ANIM_OUT_DURATION,
        force3D: true,
        onComplete() {
            completeCount++;
        }
    })

    gsap.to(refs[constants.CURRENT_IMAGE_INDEX].current, {
        x: current_position.x + (Math.random() * 100) * directions[constants.CURRENT_IMAGE_INDEX][0],
        y: current_position.y + (Math.random() * 100) * directions[constants.CURRENT_IMAGE_INDEX][1],
        opacity: 0,
        duration: constants.ANIM_OUT_DURATION,
        force3D: true,
        onComplete() {
            completeCount++;
        }
    })

    gsap.to(refs[constants.NEXT_IMAGE_INDEX].current, {
        x: next_position.x + (Math.random() * 100) * directions[constants.NEXT_IMAGE_INDEX][0],
        y: next_position.y + (Math.random() * 100) * directions[constants.NEXT_IMAGE_INDEX][1],
        opacity: 0,
        duration: constants.ANIM_OUT_DURATION,
        force3D: true,
        onComplete() {
            completeCount++;
        }
    })

    // Once all 3 images are animated out, run the callback function.
    let timer = setInterval(() => {
        if (completeCount >= 4) {
            cb();
            clearInterval(timer)
        }
    })

}

/**
 * Animates everything in
 * @param dimensions {ImageDimension} the dimensions of the "featured" image.
 * @param textRef {Ref} the ref object for the main text
 * @param refs {Array<Ref>} an array of references that correspond with all the featured images.
 */
export function animateIn(
    dimensions: ImageDimension,
    textRef: Ref<any>,
    refs: Array<Ref<any>>,
    cb: Function = () => {
    }
) {
    let previous_position = ImageHelpers.getPreviousPosition(dimensions)
    let next_position = ImageHelpers.getNextPosition(dimensions)
    let current_position = ImageHelpers.getCurrentPosition(dimensions)
    let complete_count = 0;

    // animate main text in
    gsap.to(textRef.current, {
        opacity: 1,
        delay: constants.ANIM_OUT_DURATION,
        duration: constants.ANIM_OUT_DURATION,
        onComplete() {
            complete_count++;
        }
    })
    let directions = buildRandomDirections(3, 4);

    // randomly shift directions so things won't come in on the same path.
    gsap.to(refs[constants.PREVIOUS_IMAGE_INDEX].current, {
        x: previous_position.x + (Math.random() * 100) * directions[constants.PREVIOUS_IMAGE_INDEX][0],
        y: previous_position.y + (Math.random() * 100) * directions[constants.PREVIOUS_IMAGE_INDEX][1],
        opacity: 0,
        duration: 0,
        force3D: true
    })

    gsap.to(refs[constants.CURRENT_IMAGE_INDEX].current, {
        x: current_position.x + (Math.random() * 100) * directions[constants.CURRENT_IMAGE_INDEX][0],
        y: current_position.y + (Math.random() * 100) * directions[constants.CURRENT_IMAGE_INDEX][1],
        opacity: 0,
        duration: 0,
        force3D: true,

    })

    gsap.to(refs[constants.NEXT_IMAGE_INDEX].current, {
        x: next_position.x + (Math.random() * 100) * directions[constants.NEXT_IMAGE_INDEX][0],
        y: next_position.y + (Math.random() * 100) * directions[constants.NEXT_IMAGE_INDEX][1],
        opacity: 0,
        duration: 0,
        force3D: true,
    })

    // finally animate to correct position.
    gsap.to(refs[constants.PREVIOUS_IMAGE_INDEX].current, {
        x: previous_position.x,
        y: previous_position.y,
        opacity: 1,
        duration: constants.ANIM_IN_DURATION * 0.8,
        delay: Math.random() * 2,
        force3D: true,
        onComplete() {
            complete_count++;
        }
    })

    gsap.to(refs[constants.CURRENT_IMAGE_INDEX].current, {
        x: current_position.x,
        y: current_position.y,
        opacity: 1,
        duration: constants.ANIM_IN_DURATION * 0.8,
        delay: Math.random() * 2,
        force3D: true,
        onComplete() {
            complete_count++;
        }
    })

    gsap.to(refs[constants.NEXT_IMAGE_INDEX].current, {
        x: next_position.x,
        y: next_position.y,
        opacity: 1,
        duration: constants.ANIM_IN_DURATION * 0.8,
        delay: Math.random() * 2,
        force3D: true,
        onComplete() {
            complete_count++;
        }
    })

    // Once all 3 images are animated out, run the callback function.
    let timer = setInterval(() => {
        if (complete_count >= 4) {
            cb();
            clearInterval(timer)
        }
    })
}

/**
 * Does a simple fade out then fade in an element using css
 * @param ref {Ref} the React ref to animate
 * @param fadeInTimeout {number} the delay before animating the element in
 * @param css_class_name {string} the css class that manages the fade in
 */
export function fadeElementOutIn(ref: Ref, fadeInTimeout: number, css_class_name: string) {
    ref.current.classList.remove(css_class_name);

    setTimeout(() => {
        ref.current.classList.add(css_class_name);
    }, fadeInTimeout)
}
